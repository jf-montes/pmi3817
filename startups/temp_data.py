startup_data = [{
    "id":
    "1",
    "name":
    "IZIMEAL",
    "dor":
    "Dificuldade de encontrar itens dentro da cozinha",
    "logo_url":
    "https://lh3.googleusercontent.com/UyFdOMuFNzXNOdfqR-O7IbDD7zf0iePqmzcnWa23DDepKovMQeOOHWoSNrYTilFTDCbFo8Fx5nkTJGzO-8bwxnAHhQLivYOCpl1td3uU5fi1bxq1J5QZ1J6zU0Ut-e5r27hhgGBeJJh_PMXJU8ikIMfYXzSxEcYE82qYwGy0ZXr8XPFnRqa_DQfU5pN_kN1onoGVbRIemOcOX7YaXg0XuRWuqJvDBtK8rzHSlGcRdtuFY0fykMvmBNhigdVHZUqFwUtCQ5cajH0bccSEeeglpZx6mrNYbTPzrIwatVHfRNMJ0rfHcQMsSdp8jvKAHhi7DJEjtueCPieTvfmRvldiwHVbfnSSMGzIIt72g0mzHjsoQ-UtNmqKiIrLXjMnPtvYtu3GmupRkJGxf7wQbMHr3BX-89m3FTFvUCvwsyWCSq8mbpZssV9re6CvRLCUNhbSDSSCWhTqsfjGn53swq8xEMesR-7mLMiCCc38BsCfw4oPy1_dswfon48P-ehPCgqmqSxPTpI3RGNjwfv4sS78PqJTO07bdTZ6inl2rtyiagJvlruBl46A6jlPBVqcWNC4E8K4V77EzBM5g3SWdH0ON5Z04VEV-v0R2rvrcj5Jv7yhoCCzWoYQaaJY20I3YFoqYj_e50xULpC344f62s05xWx-oryxWxKEmSnGyzmTjioYlgk-eQkY-ZOyroJ_9xJQdKsZVuaazAtkECYMgir-6c8=w739-h507-no?authuser=0"
}, {
    "id":
    "2",
    "name":
    "Motiva",
    "dor":
    "Falta de incentivo de evolução na rede de educação pública",
    "logo_url":
    "https://lh3.googleusercontent.com/OnGAtjtkWibaQGCSCv4Lvg2HFseNDf_NdpD4KXjCBqOKQ5p9sbi5zM98fj4X6OVWLsQfP-S6Tfoq1KMG1q4i6ectsUceTTOHA4fVX0IbT1mJsOs02C0CM1XqxpPMz9yX6e-8YotJscyF2XyROcBuQwc1qn1MRZQkicHBdQQSOXcdf_L6KnuIELCzDaRewBz9pAPSWnl_lt-5xSydW3wwWVhuld3xR-WR6af9T8Run0Qy8KbTlV60R13Bd4by4XMczCrql-Gr0rteKP6MLf3yKBR-1uIgNK6Wn_dJi5ZU5gIvTQXoVXjus40V2ftBsGFMRVF4sVlejM8b5j5N1VJr6w4fmkFG70PS7NMnTnoYSATT27dufjnZQ3HGqqj5SzGHrGY4oGFgt3u8PrjE1EStLQvXPanP40AA6CGb5IaYabISCNSZx3w6J85_DnHK74RuJP42Y_nlYhv3ulNrpJj6Ltkv9Hyemcb1ZjhBHI76c-KkCl0B2uOvpaTWNSXZfRUrHDHnFR84sbPT6VZfmsrB68CxzUVUgTFuvAgCgDnhFW-qVVRvgo2rHzA7r3x0sjyRtGi0feMg8MtlKArC5g-svlCkljwYMN0mhInYM7S256Wfv27uUZcIC9nDhEcBSjQANiUWQsKlEujAMvk_uNm4TEmUAiIXe0NYFVVU_GSIglMrrHG8-2hYjLc_Ly7zKXf52WWpFwfPsf9Of-owXXypWjg=w481-h456-no?authuser=0"
}, {
    "id":
    "3",
    "name":
    "RE4ME",
    "dor":
    "Dificuldade de encontrar profissionais capacitados para serviços residenciais",
    "logo_url":
    "https://lh3.googleusercontent.com/zsve4kVO9ujApOYj1mZEBRQX3vJ4Wl1hegsiSoUgRqAbFcV7g8wDxYTbMCCASThHuAOF7aOimCueZHQu26057ytnTfx6Pjo3pmRLAFFyXTxMWAayeCIQTOptvx64W6nwzZ4Nu2YTRujmcZVdu274JFCaWAEx7Pyn6U7hQG_robDwvVdFRQUOFIc0pEbpBgARvvd9-igPsnXTMjlnTz0kx-4dWYLtmXQjrm1jgOAgCmNf86HX2dRmebKGFYSOw4woOAZvjJAgRGkLlT_lkEcrZN4yqJ1wFqrCC5wbHR8qYHZZtpFd9VndNKq2kOCd7sDv2IDIFW7ZHXArCehyIMX-43G1JN4nMDZNm73p2Z2pHES82Wnd66yA_iTvuNKbDhS256A-0XvQUfIxokUO8HwAqCnRa82zBnlzF3JOqLM1hOr8YklxHbnqPG1bG4cRld24fF7c_yK2zchzveod_AG0RyY5EBMsYqwVd1D8FFE5AUL3HBO9C8L4KCEiJb2MrOyatrXdAirls_seXHc7OFTSOWQgDikYRu1V5InzYSiG8Jlw9XbvX-TrgP0zUBkYTD4G5pf2OMcb-l1IfFR9mahrw_Y301q4a_y4D6IstF2QBte7feDWdHulteXIct8vxs4ka3kvf5wgao7mQrN1Uv6x5bVhA9yi5iEtbihqu-gkvhihwockyCUhA9wtkFteRO2jzMcnwanqKb67kVRAPG8QOKM=w174-h151-no?authuser=0"
}, {
    "id":
    "4",
    "name":
    "Tempo Real",
    "dor":
    "Dificuldade de PMEs de moda e vestuário não possuem um espaço centralizado para divulgação de produtos.",
    "logo_url":
    "https://lh3.googleusercontent.com/UdJtRr_TXf8T_UVcgTBh0LQkQUAsBwoixiwr0VB7bJa0ck3uLAnecWng_lcx8RN-U-J96h43eVbUIMTvPD_7fdWUT1OkDn0qWNKqUe6M0AIm5jBxcZVLGFlx1_Jh0XMRFHQdL8tR97iGcG1CPnWM-waBmwTZh3N2iKYaODd7G9YLylfV7wyi6lQBe8J627yQydsiuKBrg8MS8-Tq_LwJ0GN8fyv33lCsQwuZrBjJWD6g0-ahEbKkhclLkrSDIQyeym6_KhI9gsOlU-sJEVoxy142mqH2sx_w4cQjj0I8lIPdc2CYLEFF8lmEbQn9c7L3Bo3ov-oaf9hHkNXlfC86si9FizIkz7MpPRlFiNKecg0lfhNVGLFqiqS8e4Qfj6i6huxbQuvr1Af2rBnLfE2QSUBF7pArcKeUEeuABz7KgIOXF66P1SyZbktDHCmZy-cr1rg1wfOvFBBXRr07Tye_oZ95r-Nhe5DaZuQA4P0G0Of1sIIISEv68TNm2SwbhKVMXYvCfGtZcDuFvv3FoKavskv5UwKIuPULLvbbXT6IGruwL3oPcqykJvBDeuOFMohfrTuJUDe_QYUlrCZ3erBx-cAPlLamwClfvmiOwlGdTw4QBKzEnB9xgdO8Ab5Bat6w0deo-CQTVmQ7X6sGa1kFW9fZhwuyWNJlGkRB6QrwMhJE0NIDOZtWI_07fUE3Gv8_n0A1dlMu3GjSdvquzv6rKPU=w226-h208-no?authuser=0"
}, {
    "id":
    "5",
    "name":
    "Crypto Pay",
    "dor":
    "As criptomoedas, na atualidade, são subutilizadas como câmbio e forma de fazer pagamentos. Um usuário não consegue pagar por produtos ou serviços usando criptoativos.",
    "logo_url":
    "https://lh3.googleusercontent.com/5eofxepFCoWWZYUaLnapPWcBGXiUB_H2458RX6nHVt5cfRA93TIb9DTH1OetHrtj6DehEyxD69GtmYIv5R_fd6LyfjC5o1LU66BQGDCiTN6APBwlZro2BSEaWAfVdKHMY53HMAhWASG6G6ATZAYXsyKLJwGet6j-glg_87UPIR5etzUd2Vdvg0GauI7rIFrkzwmBS6EJZLkGor_Pf3Tk_lfmqaezj3XAmHkV1mo19Rr6yZFeFt282TxHJ7w_W6p61R2Wx8w3Hqjbsa0Gz_MN_ytkKS6ZT19f-h3GhBV6Pk21vnqPfu8C0TCAwz06L85ZDt-hqxjEMFTzYuGUZ2xy983OiLPTpKJ7FNgd-TuO4AUVo5goejwhtZho9kolPpoEQoWC-wZOPBSuOtD-RxeaLIegsm_OXNsNu56oeMUYJer8AJoNyfD2bSeXy4ICNv2y5tEGbwXioEy7zXmGGkepNEoBCVfyXZoySJ1RZuaZTN0x2dVejHsz_TS5Ogg-66dfA0jXMsOpHQfHfFAmq0d4mYtWIvqhbd2UFErk4hcsP4b8nu_yP5dvrVt6gi0tQEE15ChTOKHkKcqZjUr6wo1oYls9CyOkgtdI75s0s_7q6qlcOqa8Krut0Q7pP-2wmqI8i1rjNgxB-w_2Cx91211iFqFiKCAuYnxNYnCY7AIPcl6w6PKpQq6TIMjJIE8vG8lmYoT7lUfyzhS3XDpqkLcR1P8=w742-h87-no?authuser=0"
}, {
    "id":
    "6",
    "name":
    "AudioLX",
    "dor":
    "Dificuldade que o usuário tem de encontrar, dentro de episódios de podcasts, determinados assuntos que são abordados de forma específica em momentos específicos de um episódio",
    "logo_url":
    "https://lh3.googleusercontent.com/JlPN36ubo3XmBbnVdOZ8RvZMtsHwsIVd-jFfXikCui0BtYg5kWaE7eugdX8A7r8eNUYNP0_bucySM5X8FQ4IgZfe4Hb5_s9riVPXqdsxSKFzIGGj9KunOnMi91lQPGHPWiFQwY8wLp3EJKgeJ-v1Ff-CL4Ail7TUbaSl4iA35DJTOMqQv5x7vrJ8L5Xr8u8oZ9awNaCWjlSb9xnobS4WfcmbIE7PRM16DgDbpD23klWi33G5lToVJ_aTgwZPMYy373yzQ1RCNcUcNeXGbiyWm3Zui6MenvtQfwpXny_pQ_Tt1LROM2HN0vtVXq7nd9jKA9LSTamXNJY6s623HS60SqoFsh2m7SxoNz2i9LYaWiezPZ3Lpi1N3miZehFsbVKc_vYhqD03NYasyVC7pUc_3MmgL6xz9PABnaae_iyQiBuAVbTiPQ4NqzDaXoAhXVyZJ4khzk7_UJdGQjEz1Fj81HfwqfuNAhCpM3BXOp_MoT_doAdJXLPK0sv5jrFirOyNyot5LjSPMAtAOYQNMBE6bcVzhDlk_0y30AboiEenRkXwuGBNs2QITswZc3J21vKEfXhr8RZnhyvuamULEazeP0vcJz50PTo-qf0CKAcIcQlpfh7wIi5QAaZZV5wEWprnn4Ms69cbyPKohCdvOJ3KKsd0wy7A7ddqXiLlxPUxpCIHEHBfBmL42G6cDg2X4lfhpgz8_0wA-CWIzPXHfOBeDSY=w607-h148-no?authuser=0"
}, {
    "id":
    "7",
    "name":
    "+ Memória",
    "dor":
    "Excesso de arquivos inúteis (ou que pelo menos se tornaram) no celular",
    "logo_url":
    "https://lh3.googleusercontent.com/NzhKggDs11IzdSkwTrVxFq_rqt3cOYMb9TuHeFlEdekoojXPAlSo_a_MpFcuiztMsklCEBebG48ZjjI-1kAiWedbjAXCS3V7IgDWwUqC3ogQQgNHo7Xc-bfXOWQciDz9HshXUs6O9WJ6ugP9TeHDcu5sv8xfAF9Ehl5CadxhpPzXOb4SMOilVCGMloyItUyg1oBUI2BUM3045xhxyqrpu9qCMv13_-_rur2lgrID_75C7C56jB0fBphNsRcXlF2RXEaTSH1GKKoq98YblhjxbyA2Uhn50-bx-8q0Zvr7u_p8JpxeYe0utX0fuhG36cUCy-0aHyTmQMpQmomR8fV236k9rdOtHWNd5dUeXT6dB1f1yrJbZU9wXGCrIWG4D6Kqg7DVGBy-dVRVVdx_vhGTdIEhPOO1hMHSL-h_M_fxkzIU6Zj-hThpY9WM3LcjY9UTJeOOdYSrm7BFAcOLpfd7JAYLxxwLq_taKtzaky46JOshI7nNNXfSJR77df419cs6TMLhWnO9uznSIwMfnBbEz9rXNMkthWu1Otxyeh3alG_fMjaZglucAjybimpPrZ3bydLh3GgEO4V36FTM1f8S5rWgEsziO0pTkCoWbBFxaszZ4b66yUfixHhFwS5nfiBy0n3x8w6A7qrrkw386TykRAVOvnphkqljcX1ky3F3T2JCojU9mzSnBG1LlVq1ITwVypJkNicBUHxnQlAjXdff7vI=w604-h208-no?authuser=0"
}]
