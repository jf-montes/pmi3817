from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.shortcuts import render
from django.http import HttpResponse
from django.urls.base import reverse_lazy
from .temp_data import startup_data
from .models import Startup, Comment, Category
from django.shortcuts import render, get_object_or_404
from django.views import generic
from .forms import StartupForm, CommentForm


#def detail_startup(request, startup_id):
#    startup = get_object_or_404(Startup, pk=startup_id)
#    context = {'startup': startup}
#    return render(request, 'startups/detail.html', context)
#    Deu certo

class StartupDetailView(generic.DetailView):
    model = Startup
    template_name = 'startups/detail.html'

#def list_startups(request):
#    startup_list = Startup.objects.all()
#    context = {'startup_list': startup_list}
#    return render(request, 'startups/index.html', context)
#    Deu certo

class StartupListView(generic.ListView):
    model = Startup
    template_name = 'startups/index.html'

def search_startups(request):
    context = {}
    if request.GET.get('query', False):
        search_term = request.GET['query'].lower()
        startup_list = Startup.objects.filter(name__icontains=search_term)
        context = {"startup_list": startup_list}
    return render(request, 'startups/search.html', context)

#def create_startups(request):
#    if request.method == 'POST':
#        form = StartupForm(request.POST)
#        if form.is_valid():
#            startup_name = form.cleaned_data['name']
#            startup_dor = form.cleaned_data['dor']
#            startup_logo_url = form.cleaned_data['logo_url']
#            startup = Startup(name=startup_name,
#                          dor=startup_dor,
#                          logo_url=startup_logo_url)
#            startup.save()
#            return HttpResponseRedirect(
#                reverse('startups:detail', args=(startup.id, )))
#    else:
#        form = StartupForm()
#        context = {'form': form}
#        return render(request, 'startups/create.html', context)

class StartupCreateView(generic.CreateView):
    model = Startup
    template_name = 'startups/create.html'
    form_class = StartupForm
    success_url = reverse_lazy('startups:index')

'''
def update_startup(request, startup_id):
    startup = get_object_or_404(Startup, pk=startup_id)

    if request.method == "POST":
        form = StartupForm(request.POST)
        if form.is_valid():
            startup.name = form.cleaned_data['name']
            startup.dor = form.cleaned_data['dor']
            startup.logo_url = form.cleaned_data['logo_url']
            startup.save()
            return HttpResponseRedirect(
                reverse('startups:detail', args=(startup.id, )))
    else:
        form = StartupForm(
            initial={
                'name': startup.name,
                'dor': startup.dor,
                'logo_url': startup.logo_url
            })

    context = {'startup': startup, 'form': form}
    return render(request, 'startups/update.html', context)
'''

class StartupUpdateView(generic.UpdateView):
    model = Startup
    fields = ['name', 'dor', 'logo_url']
    template_name = 'startups/update.html'
    forms_class = StartupForm
    success_url = reverse_lazy('startups:index')

'''
def delete_startup(request, startup_id):
    startup = get_object_or_404(Startup, pk=startup_id)

    if request.method == "POST":
        startup.delete()
        return HttpResponseRedirect(reverse('startups:index'))

    context = {'startup': startup}
    return render(request, 'startups/delete.html', context)
'''
class StartupDeleteView(generic.DeleteView):
    model = Startup
    template_name = 'startups/delete.html'
    success_url = reverse_lazy('startups:index')

def create_comment(request, startup_id):
    startup = get_object_or_404(Startup, pk=startup_id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment_author = form.cleaned_data['author']
            comment_text = form.cleaned_data['text']
            comment = Comment(author=comment_author,
                            text=comment_text,
                            startup=startup)
            comment.save()
            return HttpResponseRedirect(
                reverse('startups:detail', args=(startup_id, )))
    else:
        form = CommentForm()
    context = {'form': form, 'startup': startup}
    return render(request, 'startups/comment.html', context)

class CategoryListView(generic.ListView):
    model = Category
    template_name = 'startups/category.html'

class CategoryCreateView(generic.CreateView):
    model = Category
    template_name = 'startups/create_list.html'
    fields = ['name', 'author', 'startups']
    success_url = reverse_lazy('startups:lists')

class CategoryDetailView(generic.DetailView):
    model = Category
    template_name = 'startups/ind_category.html'