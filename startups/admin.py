from django.contrib import admin

from .models import Startup, Comment, Category

admin.site.register(Startup)
admin.site.register(Comment)
admin.site.register(Category)