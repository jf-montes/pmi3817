from django.db import models
from django.conf import settings
from django.db.models.fields import DateTimeField
from django.utils import timezone


class Startup(models.Model):
    name = models.CharField(max_length=255)
    dor = models.CharField(max_length=1000)
    logo_url = models.URLField(max_length=500, null=True)

    def __str__(self):
        return f'{self.name} ({self.dor})'


class Comment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    text = models.CharField(max_length=400)
    startup = models.ForeignKey(Startup, on_delete=models.CASCADE)
    data = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f'"{self.text}" - {self.author.username}'

class Category(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    startups = models.ManyToManyField(Startup)

    def __str__(self):
        return f'{self.name}'