from django.urls import path

from . import views

app_name = 'startups'
urlpatterns = [
    #path('', views.list_startups, name='index'),
    path('', views.StartupListView.as_view(), name='index'),

    path('search/', views.search_startups, name='search'),

    #path('create/', views.create_startups, name='create'),
    path('create/', views.StartupCreateView.as_view(), name='create'),

    #path('<int:startup_id>/', views.detail_startup, name='detail'),
    path('<int:pk>/', views.StartupDetailView.as_view(), name='detail'),

    #path('update/<int:startup_id>/', views.update_startup, name='update'),
    path('update/<int:pk>/', views.StartupUpdateView.as_view(), name='update'),

    #path('delete/<int:startup_id>/', views.delete_startup, name='delete'),
    path('delete/<int:pk>/', views.StartupDeleteView.as_view(), name='delete'),

    path('<int:startup_id>/comment/', views.create_comment, name='comment'),

    path('category/', views.CategoryListView.as_view(), name='category'),

    path('category/<int:pk>', views.CategoryDetailView.as_view(), name ='ind_category')
]