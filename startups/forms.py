from django.forms import ModelForm
from .models import Startup, Comment


class StartupForm(ModelForm):
    class Meta:
        model = Startup
        fields = [
            'name',
            'dor',
            'logo_url',
        ]
        labels = {
            'name': 'Nome da Startup',
            'dor': 'Dor da Startup',
            'logo_url': 'URL da Logo',
        }

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = [
            'author',
            'text',
        ]
        labels = {
            'author': 'Nome',
            'text': 'Comentário sobre a startup',
        }